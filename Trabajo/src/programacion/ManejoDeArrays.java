package programacion;

import java.util.ArrayList;

import datos.Asignatura;
import datos.Escuela;

public class ManejoDeArrays {

	// En el atributo universidad se almacena la informaci�n de una serie de
	// escuelas (nombre,
	// asignaturas, n�mero de alumnos y si tiene m�ster) que forman parte de la
	// universidad
	private ArrayList<Escuela> universidad;

	// En el atributo escuelas se almacenan algunas escuelas que forman parte de la
	// universidad
	private Escuela[] escuelas; // se debe usar un array de longitud 10
	private int nEscuelas; // n�mero de elementos en el array escuelas

	public ManejoDeArrays(Escuela[] escuelasDeLaUniversidad, Escuela[] escuelasDelCampus) {
		universidad = new ArrayList<Escuela>();
		if (escuelasDeLaUniversidad != null) {
			int i = 0;
			while (i < escuelasDeLaUniversidad.length && escuelasDeLaUniversidad[i] != null) {
				universidad.add(escuelasDeLaUniversidad[i]);
				i++;
			}
		}
		escuelas = escuelasDelCampus;
		nEscuelas = 0;
		// si escuelas es null, se crea un array de escuelas, de tama�o 10, vac�o
		if (escuelas == null) {
			escuelas = new Escuela[10];
			return;
		}
		// nEscuelas s�lo debe almacenar el n�mero de elementos del array que no sean
		// null
		while (nEscuelas < escuelas.length && escuelas[nEscuelas] != null)
			nEscuelas++;
	}

	public int getNEscuelas() {
		return nEscuelas;
	}

	public Escuela getEscuelaEnIndice(int indice) {
		return escuelas[indice];
	}

	public ArrayList<Escuela> getUniversidad() {
		return universidad;
	}

	// M�todo que busca un profesor en un array de profesores que tiene nProfesores
	// elementos. Si lo encuentra
	// se devuelve true y si no lo encuentra se devuelve false.
	private boolean buscarProfesor(String[] profesores, int nProfesores, String nombre) {
		for (int i = 0; i < nProfesores; i++) {
			if (profesores[i].equals(nombre)) {
				return true;
			}
		}
		return false;
	}

	// M�todo que devuelve cu�ntas asignaturas del array de Asignatura son de
	// m�ster.
	public int contarAsignaturasDeMaster(Asignatura[] listaAsignaturas) {
		int cont = 0;
		for (int i = 0; i < listaAsignaturas.length; i++) {
			if (listaAsignaturas[i] != null) {
				if (listaAsignaturas[i].isMaster()) {
					cont++;
				}
			}
		}
		return cont;
	}

	/*****************************************************************************************
	 * IMPORTANTE
	 * 
	 * ANTES DE EMPEZAR A HACER LOS EJERCICIOS, ANALIZAR LAS CLASES Asignatura y
	 * Escuela Y LOS ATRIBUTOS Y EL CONSTRUCTOR DE LA CLASE ManejoDeArrays. NO
	 * OLVIDAR QUE HAY QUE LEER BIEN LA ESPECIFICACI�N (ARCHIVO PDF) DE LA
	 * EXPLICACI�N DEL TRABAJO QUE SE ENCUENTRA EN CAMPUS VIRTUAL
	 * 
	 *****************************************************************************************/

	// TODO PRIMER ejercicio del TG1
	/**
	 * M�todo (llamado buscarAsignaturasDeProfesor) que dado el nombre de un
	 * profesor y de una escuela del campus (usar el array escuelas), indique el
	 * nombre de las asignaturas impartidas por el profesor dado en la escuela dada.
	 * Nota: se puede utilizar el m�todo buscarProfesor declarado m�s arriba.
	 * 
	 * @param profesor Nombre del profesor a buscar.
	 * @param escuela  Nombre de la escuela a buscar.
	 * @return ArrayList con el nombre de las asignaturas encontradas que cumplen el
	 *         requisito pedido o null si no hay ninguna.
	 * @throws ValorNulo Si escuela o profesor es null.
	 */

	public ArrayList<String> buscarAsignaturasDeProfesor(String profesor, String escuela) throws ValorNulo {

		ArrayList<String> asignaturaDeProfesor = new ArrayList<String>(); /*
																			 * Creamos un arrayList de tipo String, para
																			 * almacenar las asignatura en la que
																			 * pertenece el profesor para luego
																			 * devolverla al usuario
																			 */

		if (profesor == null || escuela == null) { /*
													 * Establecemos el throw que se nos pide (ValorNulo Si escuela o
													 * profesor es null)
													 */
			throw new ValorNulo();
		}

		for (int i = 0; i < nEscuelas; i++) { /* Establecemos una busqueda por el ArrayList de escuela */

			if (escuelas[i].getNombre()
					.contentEquals(escuela)) { /*
												 * Comprobamos si alguno de los nombres de los valores del Array , es
												 * igual al introducido por el m�todo para buscar
												 */

				for (int j = 0; j < escuelas[i].getnAsignaturas(); j++) {/*
																			 * Realizamos otra busqueda, pero en este
																			 * caso, en las asignaturas de la escuela
																			 */

					Asignatura[] asignatura = escuelas[i]
							.getAsignaturas();/*
												 * Guardamos el Array de la asignatura en cada posici�n i del Array
												 * escuelas, en una variable Asignatura[]
												 */

					int nProfesores = asignatura[j].getnProfesores();/*
																		 * Guardamos el n�mero de asignaciones que se
																		 * han dado en el Array ,para un posterior uso.
																		 */

					String[] profesores = asignatura[j]
							.getProfesores();/*
												 * Guardamos el Array de los profesores en cada posici�n j del Array
												 * aignatura, en una variable String[]
												 */
					boolean estaProfesor = buscarProfesor(profesores, nProfesores,
							profesor);/*
										 * Utilizamos el m�todo que se nos ha dado anteriormente, introducciendole los
										 * par�metros guardados anteriormente, guardamos el resultado, en una variable
										 * boolean.
										 */
					if (estaProfesor == true) {
						asignaturaDeProfesor.add(asignatura[j]
								.getNombre());/*
												 * Realizamos la comparaci�n de lo dado anteriormente (boolean), para
												 * introducirlo el el ArrayList<String>, creado anteriormente
												 */
					}

				}
			}

		}
		if (asignaturaDeProfesor != null) {
			return asignaturaDeProfesor;/* Devolvemos el ArrayList resultante, como se nos ha pedido. */
		} else {
			return null;
		}
	}

	// TODO SEGUNDO ejercicio del TG1
	/**
	 * M�todo (llamado buscarEscuelasConMaster) que busque cu�ntas escuelas hay en
	 * la universidad con m�ster y que, adem�s, tengan m�s de un n�mero l�mite de
	 * asignaturas de m�ster (usar el ArrayList universidad).
	 * 
	 * @param limite N�mero de asignaturas de m�ster l�mite.
	 * @return El n�mero de escuelas con m�ster y m�s del limite de asignaturas de
	 *         m�ster.
	 * @throws EscuelaNoExiste Si no hay ninguna escuela que cumpla el requisito
	 *                         pedido.
	 */
	public int buscarEscuelasConMaster(int limite) throws EscuelaNoExiste {
		int cont = 0;
		for (int i = 0; i < universidad.size(); i++) {
			if (universidad.get(i).isTieneMaster() == true
					&& contarAsignaturasDeMaster(universidad.get(i).getAsignaturas()) > limite) {
				cont++;
			}
		}
		if (cont == 0) {
			throw new EscuelaNoExiste();
		} else {
			return cont;
		}
	}

	// TODO TERCER ejercicio del TG1
	/**
	 * M�todo (llamado eliminarEscuela) que elimine una escuela de nombre dado si
	 * tiene menos de x asignaturas (usando el array escuelas). Tener en cuenta que
	 * la escuela, si est�, puede estar al principio, al final o en otra posici�n
	 * distinta a las anteriores.
	 * 
	 * @param nombre El nombre de la escuela.
	 * @param x      Valor entero.
	 * @return Si se elimin� o no la escuela.
	 * @throws ValorNulo       Si nombre es null.
	 * @throws ValorVacio      Si nombre no contiene un valor.
	 * @throws EscuelaNoExiste Si la escuela no est� en el array escuelas.
	 * 
	 */
	private void desplazarIzq(int x, int y) {
		for (int i = x + 1; i <= y; i++) {
			escuelas[i - 1] = escuelas[i];
		}
	}

	private void eliminar(int indice) {
		if (nEscuelas != 0) {
			if (indice >= 0 && indice < nEscuelas) {
				if (indice == nEscuelas - 1) {
					nEscuelas--;
				} else {
					desplazarIzq(indice, nEscuelas - 1);
					nEscuelas--;
				}
			}
		}
	}

	public boolean eliminarEscuela(String nombre, int x) throws ValorNulo, ValorVacio, EscuelaNoExiste {
		if (nombre == null)
			throw new ValorNulo();
		if (nombre.length() == 0)
			throw new ValorVacio();
		for (int i = 0; i < nEscuelas; i++) {
			if (escuelas[i].getNombre().equals(nombre)) {
				if (escuelas[i].getnAsignaturas() < x) {
					eliminar(i);
					return true;
				} else {
					return false;
				}
			}
		}
		throw new EscuelaNoExiste();
	}

	// TODO CUARTO ejercicio del TG1
	/**
	 * M�todo (llamado buscarEscuelaConMenosAsignaturas) que busque el nombre de la
	 * escuela de la universidad (usar el ArrayList universidad), que tiene menos
	 * asignaturas de m�ster. Nota: se puede utilizar el m�todo
	 * contarAsignaturasDeMaster declarado m�s arriba.
	 * 
	 * @return String con el nombre de la escuela o null si ninguna escuela tiene
	 *         asignaturas de m�ster.
	 * @throws ValorVacio Si el ArrayList universidad est� vac�o.
	 */
	public String buscarEscuelaConMenosAsignaturas() throws ValorVacio {
		if (universidad.size() == 0) {
			throw new ValorVacio();
		}
		int cont = 0;
		Asignatura[] menor = universidad.get(0).getAsignaturas();
		String nombre = universidad.get(0).getNombre();

		for (int i = 0; i < universidad.size(); i++) {
			if (contarAsignaturasDeMaster(universidad.get(i).getAsignaturas()) != 0) {
				menor = universidad.get(i).getAsignaturas();
				nombre = universidad.get(i).getNombre();
				cont++;
			}
		}
		if (cont == 0) {
			return null;
		}
		
		for (int j = 0 ; j< universidad.size();j++) {
			if(contarAsignaturasDeMaster(universidad.get(j).getAsignaturas())< contarAsignaturasDeMaster(menor)
					&&contarAsignaturasDeMaster(universidad.get(j).getAsignaturas())!=0) {
				menor = universidad.get(j).getAsignaturas();
				nombre = universidad.get(j).getNombre();
			}
		}
		return nombre;

	}

	// TODO QUINTO ejercicio del TG1
	/**
	 * M�todo (llamado insertarEscuela) que inserte una nueva escuela en el conjunto
	 * de escuelas del campus (usar el array escuelas), si no est�.
	 * 
	 * @param escuela La escuela (objeto Escuela) a insertar en el array.
	 * @param indice  �ndice donde se inserta la escuela en el array, si no est�.
	 * @return Si se pudo o no insertar.
	 * @throws ValorNulo Si escuela es null.
	 * @trhows EstaLleno Si el array escuelas est� lleno.
	 * @throws IndexOutOfBoundsException Si indice no est� en el rango 0 <= indice
	 *                                   <= nEscuelas.
	 */
	private void desplazarDcha(int x, int y) {
		for (int i = y; i >= x + 1; i--) {
			escuelas[i] = escuelas[i - 1];
		}
	}

	private boolean buscarEscuela(Escuela escuela) {
		for (int i = 0; i < escuelas.length; i++) {
			if (escuelas[i].equals(escuela)) {
				return true;
			}
			return false;
		}
		return false;
	}

	public boolean insertarEscuela(Escuela escuela, int indice) throws ValorNulo, EstaLleno, IndexOutOfBoundsException {
		if (escuela == null) {
			throw new ValorNulo();
		}

		if (nEscuelas == escuelas.length) {
			throw new EstaLleno();
		}

		if (indice < 0 || indice > nEscuelas) {
			throw new IndexOutOfBoundsException();
		}

		if (buscarEscuela(escuela)) {
			return false;
		}

		if (nEscuelas == 0) {
			escuelas[indice] = escuela;
			nEscuelas++;
		} else if (indice == nEscuelas) {
			escuelas[indice] = escuela;
			nEscuelas++;
		} else {
			desplazarDcha(indice, nEscuelas);
			escuelas[indice] = escuela;
			nEscuelas++;
		}
		return true;
	}

	// TODO SEXTO ejercicio del TG1
	/**
	 * M�todo (llamado ordenarUniversidad) que ordena la colecci�n universidad
	 * siguiendo el siguiente criterio: de menor a mayor por n�mero de estudiantes.
	 * A igual de n�mero de estudiantes, de menor a mayor por el nombre de la
	 * escuela (usar el ArrayList universidad).
	 * 
	 * @throws ValorNulo  Si universidad es null.
	 * @throws ValorVacio Si universidad est� vac�o.
	 */
	public void ordenarUniversidad() throws ValorNulo, ValorVacio {
		if (universidad == null) {
			throw new ValorNulo();
		}
		if (universidad.isEmpty()) {
			throw new ValorVacio();
		}
		for (int i = 0; i < universidad.size(); i++) {
			int posMenor = buscarMenor(i);
			Escuela temp = universidad.get(i);
			universidad.set(i, universidad.get(posMenor));
			universidad.set(posMenor, temp);

		}
	}

	// creo el metodo de buscarMenor con las idicaciones del enunciado para despues
	// para despues usarlo en el metodo de ordenar para que me ordene de menor a
	// mayor
	// y creo el metedo de ordenar como hemos visto en clase, a�adiendo los if para
	// los
	// throws
	private int buscarMenor(int i) {
		Escuela menor = universidad.get(i);
		int posMenor = i;
		for (int j = i + 1; j < universidad.size(); j++) {
			if (universidad.get(j).getnAlumnos() < menor.getnAlumnos()) {
				menor = universidad.get(j);
				posMenor = j;
			} else if (universidad.get(j).getnAlumnos() == menor.getnAlumnos()) {
				if (universidad.get(j).getNombre().compareTo(menor.getNombre()) < 0) {
					menor = universidad.get(j);
					posMenor = j;
				}
			}

		}
		return posMenor;
	}

}