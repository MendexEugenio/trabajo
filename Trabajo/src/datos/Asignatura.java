package datos;

public class Asignatura {
	private String nombre; // nombre de la asignatura
	private String[] profesores; // conjunto de profesores
	private int nProfesores; // n�mero de elementos del array profesores
	private boolean master; // es una asignatura de m�ster o no
	
	public Asignatura(String nombre, String[] profesores, int nProfesores, boolean master) {
		super();
		this.nombre = nombre;
		this.profesores = profesores;
		this.nProfesores = nProfesores;
		this.master = master;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String[] getProfesores() {
		return profesores;
	}

	public void setProfesores(String[] profesores) {
		this.profesores = profesores;
	}

	public int getnProfesores() {
		return nProfesores;
	}

	public void setnProfesores(int nProfesores) {
		this.nProfesores = nProfesores;
	}
	
	public boolean isMaster() {
		return master;
	}

	public void setMaster(boolean master) {
		this.master = master;
	}

	
}
