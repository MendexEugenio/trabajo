package datos;

public class Escuela {
	private String nombre; // nombre de la escuela
	private Asignatura[] asignaturas; // lista de asignaturas
	private int nAsignaturas; // n�mero de elementos del array asignaturas
	private int nAlumnos; // n�mero de alumnos de la escuela
	private boolean tieneMaster; // true si la escuela tiene m�ster y false si no lo tiene
	
	public Escuela(String nombre, Asignatura[] asignaturas, int nAsignaturas, int nAlumnos, boolean tieneMaster) {
		super();
		this.nombre = nombre;
		this.asignaturas = asignaturas;
		this.nAsignaturas = nAsignaturas;
		this.nAlumnos = nAlumnos;
		this.tieneMaster = tieneMaster;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Asignatura[] getAsignaturas() {
		return asignaturas;
	}

	public void setAsignaturas(Asignatura[] asignaturas) {
		this.asignaturas = asignaturas;
	}

	public int getnAsignaturas() {
		return nAsignaturas;
	}

	public void setnAsignaturas(int nAsignaturas) {
		this.nAsignaturas = nAsignaturas;
	}

	public int getnAlumnos() {
		return nAlumnos;
	}

	public void setnAlumnos(int nAlumnos) {
		this.nAlumnos = nAlumnos;
	}

	public boolean isTieneMaster() {
		return tieneMaster;
	}

	public void setTieneMaster(boolean tieneMaster) {
		this.tieneMaster = tieneMaster;
	}
	
	
	
}
