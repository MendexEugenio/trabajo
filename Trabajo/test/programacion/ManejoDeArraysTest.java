package programacion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import datos.Asignatura;
import datos.Escuela;

public class ManejoDeArraysTest {

	private ArrayList<Escuela> universidad;
	private Escuela[] escuelas;
	private Escuela escuela1, escuela2, escuela3, escuela4, escuela5;
	private Asignatura asignatura1, asignatura2, asignatura3, asignatura4, asignatura5, asignatura6, asignatura7,
			asignatura8;

	@Before
	public void setUp() throws Exception {
		// ejemplo de posibles datos de pruebas. Los estudiantes pueden y deben hacer
		// sus propios datos de pruebas para probar los distintos casos que se puedan
		// dar en
		// los m�todos que vayan a probar.
		asignatura1 = new Asignatura("CEyO",
				new String[] { "Faraday", "Maxwell", "Lenz", "Newton", "Gauss", "", "", "", "", "" }, 5, true);

		asignatura2 = new Asignatura("Programaci�n",
				new String[] { "Neumann", "Newton", "Wosniak", "Eugenio", "Carmen", "", "", "", "", "" }, 5, true);

		asignatura3 = new Asignatura("El�ctronica B�sica",
				new String[] { "Lenz", "Norton", "Fibonacci", "", "", "", "", "", "", "" }, 3, false);

		asignatura4 = new Asignatura("C�lculoII", new String[] { "Vinci", "Leonardo", "Gauss", "Pedro", "Kichori",
				"Eugenio", "Hugo", "Luisa", "Domingo", "", "", "" }, 9, false);

		asignatura5 = new Asignatura("Circu�tos",
				new String[] { "Thevenin", "Norton", "Faraday", "Ohm", "", "", "", "", "", "" }, 4, true);

		asignatura6 = new Asignatura("Estad�stica y Procesos Estoc�sticos",
				new String[] { "Eugenio", "Watt", "", "", "", "", "", "" }, 2, false);

		asignatura7 = new Asignatura("Inform�tica",
				new String[] { "Ohm", "Amp�re", "Neumann", "Leo", "", "", "", "", "", "" }, 4, true);

		asignatura8 = new Asignatura("F�sica Nuclear", new String[] { "Curie", "", "", "", "", "", "", "", "", "" }, 1,
				false);

		escuela1 = new Escuela("Escuela 1",
				new Asignatura[] { asignatura1, asignatura3, asignatura4, asignatura6, null, /* 1 de Master */
						null, null, null, null, null },
				4, 110, true);

		escuela2 = new Escuela("Escuela 2", new Asignatura[] { asignatura2, asignatura5, asignatura4, asignatura7, /*
																													 * 2
																													 * de
																													 * Master
																													 */
				asignatura8, null, null, null, null, null }, 5, 115, false);

		escuela3 = new Escuela("Escuela 3", new Asignatura[] { asignatura8, asignatura5, asignatura2, asignatura1,
				asignatura4, asignatura3, null, null, null, null }, 6, 75, true);

		escuela4 = new Escuela("Escuela 4", new Asignatura[] { asignatura1, asignatura2, asignatura3, asignatura4,
				asignatura5, asignatura7, asignatura8, null, null, null }, 7, 150, false);

		escuela5 = new Escuela("Escuela 5",
				new Asignatura[] { asignatura4, asignatura6, null, null, null, null, null, null }, 2, 50, true);
		/* Creaci�n de los ditintos ArrayLists que se necesitaran para el test */

		universidad = new ArrayList<Escuela>();
		universidad.add(escuela1);
		universidad.add(escuela2);
		universidad.add(escuela3);
		universidad.add(escuela4);
		universidad.add(escuela5);

		escuelas = new Escuela[10];
		escuelas[0] = escuela1;
		escuelas[1] = escuela2;
		escuelas[2] = escuela3;
		escuelas[3] = escuela4;
		escuelas[4] = escuela5;
		// las posiciones del array sin escuelas, deben estar a null
		escuelas[5] = escuelas[6] = escuelas[7] = escuelas[8] = escuelas[9] = null;
	}

	// Aqu� van todos los tests del PRIMER ejercicio del TG1
	@Test
	public void testBuscarAsignaturaDeProfesorTiene() throws ValorNulo { /* Creaci�n de m�todo */

		ManejoDeArrays Arrays = new ManejoDeArrays(escuelas, escuelas); /* Creaci�n del objeto para su utilizaci�n */

		ArrayList<String> asignaturasObtenidas = Arrays.buscarAsignaturasDeProfesor("Ohm",
				"Escuela 3"); /*
								 * Implementaci�n del m�todo a probar ,introduciendole los parametros que se
								 * nesecitan
								 */
		ArrayList<String> asignaturasEsperadas = new ArrayList<String>(); /*
																			 * Creaci�n del ArrayList con el resultado
																			 * que debera dar el test
																			 */
		asignaturasEsperadas.add("Circu�tos"); /* Introducci�n de los par�metros esperados en el ArrayList */

		assertTrue(asignaturasObtenidas.equals(asignaturasEsperadas));/*
																		 * Comparaci�n de lo obtenido en el test, con lo
																		 * esperado de ese test ,si coinciden.
																		 */

	}

	@Test(expected = ValorNulo.class) /*Si pruebas una excepci�n,para probar esa excepci�n.*/
	public void testBuscarEscuelaOProfesorNulo() throws ValorNulo {   /*Declarar el test como un m�todo, y si tiene una excepci�n declararla con un throws*/
		ManejoDeArrays misEscuelas = new ManejoDeArrays(escuelas, escuelas); /*Creas un objeto para probar los m�todos creados.*/
		misEscuelas.buscarAsignaturasDeProfesor(null, "escuela3");/*Declaraci�n del m�todo a probar*/
	}/*En este test no hace falta un assert, ya que pruebas que se lance la excepci�n.*/

	// Aqu� van todos los tests del SEGUNDO ejercicio del TG1
	
	@Test
	public void testBuscarEscuelasConMaster2() throws EscuelaNoExiste {
		ManejoDeArrays misEscuelas = new ManejoDeArrays (escuelas,
				escuelas);
		int cont = 1;
		int resultado = misEscuelas.buscarEscuelasConMaster(2);
				 assertEquals(cont, resultado);
				}
	
	
	
	 @Test (expected = EscuelaNoExiste.class)
	public void testBuscarEscuelasConMasterNoExiste() throws EscuelaNoExiste {
		
			ManejoDeArrays misEscuelas = new ManejoDeArrays (escuelas,escuelas);
			misEscuelas.buscarEscuelasConMaster(5);

	 }


	// Aqu� van todos los tests del TERCER ejercicio del TG1
	@Test (expected = ValorVacio.class)
	public void testEliminarEscuelaValorVacio() throws ValorNulo,ValorVacio,EscuelaNoExiste{
		ManejoDeArrays arrays = new ManejoDeArrays(escuelas, escuelas);
		arrays.eliminarEscuela("", 8);
	} 
	
	@Test (expected = EscuelaNoExiste.class)
	public void testEliminarEscuelaNoExiste() throws ValorNulo,ValorVacio,EscuelaNoExiste{
		Escuela[]  escuela = new Escuela[0]; 
		ManejoDeArrays arrays = new ManejoDeArrays(escuela, escuela);
		arrays.eliminarEscuela("Pedro", 8);

	}

	
	
	// Aqu� van todos los tests del CUARTO ejercicio del TG1
	@Test/*Falla*/
	public void testbuscarEscuelaConMenosAsignaturas() throws ValorVacio {/*Declarar el test como un m�todo, y si tiene una excepci�n declararla con un throws*/
		ManejoDeArrays miEscuela = new ManejoDeArrays(escuelas, escuelas);/*Creas un objeto para probar los m�todos creados.*/
		String escuelaObtenida = miEscuela.buscarEscuelaConMenosAsignaturas();/*Guarda en una variable el resultado de la implementaci�n del m�todo a usar.
		 																		Luego se comparar� con un equals.*/
		boolean prueba = escuelaObtenida.equals("Escuela 1");/*Como nos encontramos ante un String, lo compararemos con un equals y lo que est�s buscando como resultado.*/
		assertTrue(prueba);/*Comprobaci�n del booleano obtenido para comprobar si es correcto.*/
	}
	
	
	
	
	@Test(expected = ValorVacio.class)
	public void testBuscarEscuelaConMenosAsignaturasValorVacio() throws ValorVacio {
		Escuela[] escuelas1 = new Escuela[10];
		ManejoDeArrays escuela = new ManejoDeArrays(escuelas1, escuelas1);
		escuela.buscarEscuelaConMenosAsignaturas();
	}

	// Aqu� van todos los tests del QUINTO ejercicio del TG1
	@Test (expected =IndexOutOfBoundsException.class)
	public void testInsertarEscuelaIndexOutOfBoundsException()throws ValorNulo, EstaLleno, IndexOutOfBoundsException{
		escuela1 = new Escuela("Escuela 1",  new Asignatura[]{asignatura1, asignatura3, asignatura4, asignatura6, null, null, null, null, null, null}, 4, 110, true);
		ManejoDeArrays Arrays = new ManejoDeArrays(escuelas, escuelas); 
		Arrays.insertarEscuela(escuela1,-1);
	}
	@Test 
	public void testA�adirUnaEscuelaCorrectamente() throws ValorNulo, EstaLleno, IndexOutOfBoundsException {
	Escuela escuela4 =new Escuela("Escuela 1",  
			new Asignatura[]{asignatura1, asignatura3, asignatura4, asignatura6, null, null, null, null, null, null}, 5, 151, true);
	ManejoDeArrays misEscuelas = new ManejoDeArrays (escuelas, escuelas); 
	misEscuelas.insertarEscuela(escuela4, 1);
	}

	// Aqu� van todos los tests del SEXTO ejercicio del TG1
	@Test
	public void testOrdenarUniversidadTresUniversidades() throws ValorNulo, ValorVacio {
	Escuela[] escuela = {
		escuela1 = new Escuela("Escuela 1",  
				new Asignatura[]{asignatura1, asignatura3, asignatura4, asignatura6, null, null, null, null, null, null}, 4, 110, true), 
		escuela2 = new Escuela("Escuela 2", 
				new Asignatura[]{asignatura2, asignatura5, asignatura4, asignatura7, asignatura8, null, null, null, null, null}, 5, 115, false), 
		escuela3 = new Escuela("Escuela 3",   
				new Asignatura[]{asignatura8, asignatura5, asignatura2, asignatura1, asignatura4, asignatura3, null, null, null, null}, 6, 75, true),
	};
	ManejoDeArrays misEscuelas = new ManejoDeArrays (escuela,escuela);
	misEscuelas.ordenarUniversidad();
	assertEquals(escuela3,misEscuelas.getUniversidad().get(0));
	assertEquals(escuela1,misEscuelas.getUniversidad().get(1));
	assertEquals(escuela2,misEscuelas.getUniversidad().get(2));
	//este es un test que comprueba un caso cualquiera porque cree mi array de Escuela llamado
	//escuela y le a�adi tres escuelas y los ordende segun el numero de estudiantes 
	
	
}
	@Test(expected = ValorVacio.class)
	public void testOrdenarUniversidadesValorNulo() throws ValorVacio,ValorNulo {
		Escuela[] escuelas1 = null;
		ManejoDeArrays escuela = new ManejoDeArrays(escuelas1, escuelas1);
		escuela.ordenarUniversidad();
	}
}
